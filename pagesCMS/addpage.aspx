﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="addpage.aspx.cs" Inherits="pagesCMS.addpage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<label>Page title</label>
<asp:TextBox runat="server" ID="page_title"></asp:TextBox>
    <label>Page Content</label>
    <asp:TextBox runat="server" ID="page_content"></asp:TextBox>
    <asp:SqlDataSource runat="server" ID="page_add"
        connectionstring="<%$ConnectionStrings:final_sql_con %>"></asp:SqlDataSource>
   <asp:Button runat="server" Text="add" OnClick ="Insert_page" />
    <div runat ="server" id="debug"></div>
</asp:Content>
