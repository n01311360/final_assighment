﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;

namespace pagesCMS
{
    public partial class Page : System.Web.UI.Page
    {
        public string pageid
        {
            get { return Request.QueryString["pageid"]; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string query = "select * from pages where pageid =" + pageid;

            debug.InnerHtml = query;

            page_select.SelectCommand = query;
            //once we've grabbed that information from the database, bind it to variables (strings) and show it
            //to the h3 and p tags

            //put the information from the database into some kind of class where we can access it
            DataView pageview = (DataView)page_select.Select(DataSourceSelectArguments.Empty);
            DataRowView pagerowview = pageview[0];

            //Once we have access to that info we can put that into the page
            page_content.InnerHtml = pagerowview["pagecontent"].ToString();
            page_title.InnerHtml = pagerowview["pagetitle"].ToString();


        }

        protected void DeletePage(object sender, EventArgs e)
        {
            string query = "delete from pages where pageid = " + pageid;
            debug.InnerHtml = query;
            page_delete.DeleteCommand = query;
            page_delete.Delete();
        }
    }
        
 


}