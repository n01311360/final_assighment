﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="editpage.aspx.cs" Inherits="pagesCMS.editpage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    
<asp:SqlDataSource runat="server" id="page_select"
        connectionstring="<%$ConnectionStrings:final_sql_con %>"></asp:SqlDataSource>

<asp:SqlDataSource runat="server" id="page_edit"
        connectionstring="<%$ConnectionStrings:final_sql_con %>"></asp:SqlDataSource>

<h3 runat="server" id="title">Edit</h3>

    <div class="inputrow">
        <label>Page title</label>
    <asp:TextBox runat="server" id="page_title"></asp:TextBox>
    </div>

    <div class="inputrow">
        <label>page title</label>
    <asp:TextBox runat="server" id="page_content"></asp:TextBox>
    </div>

    <asp:Button runat="server" Text="Edit" OnClick="Update_Page" />

    <div class="querybox" id="debug" runat="server"></div>
</asp:Content>
