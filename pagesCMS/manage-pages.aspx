﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="manage-pages.aspx.cs" Inherits="pagesCMS.manage_pages" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <asp:SqlDataSource runat="server"
        connectionstring="<%$ConnectionStrings:final_sql_con %>" id="pages_select"></asp:SqlDataSource>
    <a href="addpage.aspx">Add a New Page</a>
    <asp:DataGrid runat="server" ID="pages_list"></asp:DataGrid>
</asp:Content>
