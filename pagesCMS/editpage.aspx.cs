﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;

namespace pagesCMS
{
    public partial class editpage : System.Web.UI.Page
    {
        public string pageid
        {
            get { return Request.QueryString["pageid"]; }
        }        

        protected void Page_Load(object sender, EventArgs e)
        {
            
           
        }
        protected override void OnPreRender(EventArgs e)
        {

            //string query = "select pageid, pagettile, pagecontent from pages where pageid = " + pageid;
            //page_edit.SelectCommand = query;

            string query = "select * from pages where pageid =" + pageid;

            page_select.SelectCommand = query;
            //once we've grabbed that information from the database, bind it to variables (strings) and show it
            //to the h3 and p tags

            //put the information from the database into some kind of class where we can access it
            DataView pageview = (DataView)page_select.Select(DataSourceSelectArguments.Empty);
            DataRowView pagerowview = pageview[0];

            //Once we have access to that info we can put that into the page
            page_content.Text = pagerowview["pagecontent"].ToString();
            page_title.Text = pagerowview["pagetitle"].ToString();
        }
        protected void Update_Page(object sender, EventArgs e)
        {
            string title = page_title.Text;
            string content = page_content.Text;

            

            string query = "UPDATE pages set pagetitle = '" + title + "',pagecontent = '" + content + "' WHERE pageid= " + pageid;

           
            //string query = "insert into pages (pagetitle,pagecontent) values ('" + title + "','" + content + "')";

            page_edit.UpdateCommand = query;
            page_edit.Update();
            debug.InnerHtml = query;
        }
    }
}