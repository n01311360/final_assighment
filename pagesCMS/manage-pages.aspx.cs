﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;

namespace pagesCMS
{
    public partial class manage_pages : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            pages_select.SelectCommand = "select * from pages";
            //change it so that they link to one particular page in the page name
            pages_list.DataSource = pages_manual_bind(pages_select);
            pages_list.DataBind();
        }

        //build a rendering loop
        protected DataView pages_manual_bind(SqlDataSource src)
        {
            DataTable mytbl;
            DataView myview;
            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;
            foreach (DataRow row in mytbl.Rows)
            {
                //<a href="google.com">ninjas</a>
                string pageid = row["pageid"].ToString();
                row["pagetitle"] = "<a href=\"Page.aspx?pageid="+pageid+"\">"+ row["pagetitle"] + "</a>";
            }
            mytbl.Columns.Remove("pageid");
            myview = mytbl.DefaultView;

            return myview;
        }
        
    }
}