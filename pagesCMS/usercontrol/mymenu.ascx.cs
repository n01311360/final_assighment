﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace pagesCMS.usercontrol
{
    public partial class mymenu : System.Web.UI.UserControl
    {

        private string basequery = "SELECT pageid, pagetitle from pages";

        protected void Page_Load(object sender, EventArgs e)

        {
            pages_menu.SelectCommand = basequery;
            DataView pageview = (DataView)pages_menu.Select(DataSourceSelectArguments.Empty);

            string menuguts = "";
            foreach (DataRowView row in pageview)
            {
                string id = row["pageid"].ToString();
                string title = row["pagetitle"].ToString();
                menuguts += "<li><a href =\"page.aspx?pageid=" + id + "\">" + title + "</a></li>";

            }
            menucontainer.InnerHtml = menuguts;
        }

    }
}