﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Page.aspx.cs" Inherits="pagesCMS.Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h3 runat="server" id="page_title"></h3>
    <p id="page_content" runat="server"></p>
    <div id="debug" runat="server"></div>
    <asp:SqlDataSource runat="server" connectionstring="<%$ConnectionStrings:final_sql_con %>" ID="page_select"></asp:SqlDataSource>
    
    <asp:SqlDataSource runat="server" connectionstring="<%$ConnectionStrings:final_sql_con %>" ID="page_delete"></asp:SqlDataSource>
    
    <asp:Button runat="server" OnClick="DeletePage"  Text="delete"/>
    <a href="EditPage?pageid=<% Response.Write(this.pageid); %>">Edit</a>

    
    
    </asp:Content>
